package part6;

public interface INotificationService {

    void sendNotification(String email, String message);
}
# Del 7 - I/O


I denne oppgåva skal du implementera klassen [CrewScheduleWriter](./CrewScheduleWriter.java) som skriv ein mannskapsplan til fil. Du treng ikkje å ha løyst [del 3](../part3/part3.md) for å gjera dette, sjølv om testing av klassen då kanskje ikkje gir dei rette resultata.

Fyll ut følgjande metode:

1. `writeCrewScheduleForFlight(ICrewSchedule crewSchedule, IFlight flight, OutputStream outputStream)`. Skriv ut besetningsplanen for den gitte flyginga basert på formatet gitt i JavaDoc og i [ExampleFile](./example.txt)
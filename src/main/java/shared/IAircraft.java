package shared;

public interface IAircraft {
  // Returns the type of the aircraft
  public String getType();
}